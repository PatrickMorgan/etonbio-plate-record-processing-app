<?php

////////////////////////////////
//PRP app methods
///////////////////////////////

    ////////////////////////////////
    //View Methods
    ///////////////////////////////
    function render_view($view){
        $stache = new Mustache_Engine;
        $view_template = $view["view"];
        $view["view"] = "";
        $rendered = $stache->render($view_template,$view);

        return $rendered;
    }

    function load_view($view,$data){
        //fetch view layout
        $fetch_view = fetch_resource(_VIEWDIR."{$view}.view");

        //load dependencies
        //fetch css
        $fetch_css = fetch_resource(_UILIBDIR."css/sweetalert.min.css");
        $fetch_css .= fetch_resource(_CSSDIR."prp.css");
/*FOR DEV*/$fetch_css .= fetch_resource("http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css");/*FOR DEV*/
        //fetch js

        $fetch_js = fetch_resource(_UILIBDIR."js/jquery.min.js");
/*FOR DEV*/$fetch_js .= fetch_resource("http://code.jquery.com/ui/1.9.2/jquery-ui.js");/*FOR DEV*/
        $fetch_js .= fetch_resource(_UILIBDIR."js/sweetalert.min.js");

        //load app js
        $fetch_js .= fetch_resource(_JSDIR."prp.js");
        $fetch_js .= fetch_resource(_JSDIR."{$view}.js");

        //render view
        $render_view = render_view(array(
          "view"=>$fetch_view,
          "css"=>$fetch_css,
          "js"=>$fetch_js,
          "viewdir"=>_WEBVIEWDIR,
          "jsidir"=>_JSIDIR,
          "data"=>(isset($data))?$data:null
        ));

        //return
        return $render_view;
    }

    function fetch_resource($resource){
        //load resource for view
        //@var string $resource Should be absolute path to resource ex: _PRPDIR."/resource/location/file.txt" or http://resource.com/location/file.txt
        //
        //@return string | boolean $resource_data

        if(file_exists($resource) || substr($resource,0,4) == "http"){
            $resource_data = file_get_contents($resource);
            return $resource_data;
        }else{
            return false;
        }
    }

    ////////////////////////////////
    //DB Methods
    ///////////////////////////////

    function connect_db($db_config){
        //connect to DB

        $db = new mysqli(
          /* server */$db_config["host"]
          ,/* user */$db_config["usr"]
          ,/* pass */$db_config["pwd"]
          ,/*db*/$db_config["db"]
          // ,/* port */$db_config["port"]
        );
        if($db->connect_errno > 0){
            $mysqli->set_charset("utf8");
            return array(
                "connect_fail"=>true,
                "db"=>$db
            );
        }else{
            return $db;
        }

    }

    function sanitize_str($str,$db){
        //sanitize user input for use with db query
        $sanitized = $db->real_escape_string($str);

        return $sanitized;
    }

    function multi_qry($qry,$db){

    	$rows = array();
		$res = $db->multi_query($qry);

        if($db->errno > 0){
            return false;
        }

		do {
			if ($res = $db->store_result()) {

				while($row = $res->fetch_array(MYSQLI_ASSOC)){
		    		$rows[] = $row;
				}

				$res->free();
			}
		} while ($db->more_results() && $db->next_result());

		return $rows;
    }

    ////////////////////////////////
    //User Methods
    ///////////////////////////////

    function fetch_user_list($branch,$db){
        //get employee list for branch
        //@var string $branch   Should be branch ID for office
        //@var object $db   Should be MYSQLI database object

        switch($branch){
            case "SD":
                $branch_loc = "San Diego";
            break;
            case "NJ":
                $branch_loc = "New Jersey";
            break;
            case "MD":
                $branch_loc = "Boston";
            break;
            case "NC":
                $branch_loc = "North Carolina";
            break;
        }

        $user_qry = "SELECT id,first_name,last_name FROM employees WHERE office_location='{$branch_loc}' ORDER BY last_name ASC";

        $run_qry = $db->query($user_qry);

        if($run_qry->num_rows > 0){
            $users = array();
            while($user = $run_qry->fetch_array(MYSQLI_ASSOC)){
                $users[] = $user;
            }

            return $users;
        }else{
            return false;
        }

    }

    function fetch_user($id,$db){
        $user_qry = "SELECT first_name,last_name FROM employees WHERE id={$id}";
        $run_qry = $db->query($user_qry);

        if($run_qry->num_rows > 0){
            $users = array();
            $user = $run_qry->fetch_array(MYSQLI_ASSOC);

            return $user["first_name"]." ".$user["last_name"];
        }else{
            return "Failed To Fetch Technician";
        }
    }

    ////////////////////////////////
    //Order Methods
    ///////////////////////////////

    //check if service table has record for order number and current service date
    function order_has_service($order_no,$service_date,$plate_designator,$plate_no,$db){
        $order_qry = "SELECT id FROM servicestable WHERE OrderNumber='{$order_no}' AND ServiceDate='{$service_date}' AND plateNumber='{$plate_no}' AND PlateRec='{$plate_designator}'";

        $run = $db->query($order_qry);

        if($run->num_rows > 0){
            return true;
        }else{
            return false;
        }
    }
    function create_order_service($order_no,$plate_designator,$plate_no,$service_date,$db){
        @session_start();
        $tech = $_SESSION['prp_tech'];
        $order_qry  = "INSERT INTO servicestable SET PlateRec='{$plate_designator}',ServiceDate='{$service_date}', OrderNumber='{$order_no}', TechName='{$tech}'";

        $run = $db->query($order_qry);

        if($db->errno < 1){
            return true;
        }else{
            return $db->error;
        }
    }
    function update_order_designator($plate_designator,$order_no,$service_date,$db){
            //update ordertable
            $designator_qry  = "UPDATE ordertable SET PlateRec='{$plate_designator}' WHERE OrderNumber='{$order_no}'";

            $run = $db->query($designator_qry);
            if($db->errno > 0){
                $err = true;
                $error_data[] = array(
                    "qry"=>$sample_qry,
                    "db"=>$db,
                    "run"=>$run
                );
                return array(
                    $err,$error_data
                );
            }

            //update servicestable

            $services_qry  = "UPDATE servicestable SET plateRec='{$plate_designator}' WHERE ServiceDate='{$service_date}' AND OrderNumber='{$order_no}'";
            $run = $db->query($services_qry);

            if($db->errno > 0){
                $err = true;
                $error_data[] = array(
                    "qry"=>$sample_qry,
                    "db"=>$db,
                    "run"=>$run
                );
                return array(
                    $err,$error_data
                );
            }
    }
    function gen_order_highlight($order_no){
        $hex = strrev($order_no);
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));
        $alpha = "0.5";

        $rgba = array($r, $g, $b, $alpha);
        $rgba = implode(",", $rgba);

        $highlight_css = ".or{$order_no}{background-color:rgba({$rgba});}";

        return $highlight_css;
    }

    ////////////////////////////////
    //Plate Methods
    ///////////////////////////////

    function get_plate_no($plate_id,$order_no,$db){
        $plate_qry = "SELECT DISTINCT plateID FROM platerecordprocessing WHERE OrderNumber={$order_no} ORDER BY id ASC";
        $run = $db->query($plate_qry);

        if($db->errno > 0){
            return 1;
        }
        if($run->num_rows < 1){
            return 1;
        }

        $plate_no = 1;

        while($well = $run->fetch_array(MYSQLI_ASSOC)){
            if($well["plateID"] == $plate_id){
                return $plate_no;
            }
            $plate_no++;
        }

        return $plate_no;
    }

    function gen_plate($date,$tech,$plate,$db){
        $well_ct = 96;
        $well_no = 1;
        $wells = array();

        while($well_ct > 0){
            //make a well

            //get column and row
            switch(true){
                case $well_no < 13:
                    $well_col = "A";
                    $well_id_no = $well_no;
                break;
                case $well_no > 12 && $well_no < 25:
                    $well_col = "B";
                    $well_id_no = $well_no - 12;
                break;
                case $well_no > 24 && $well_no < 37:
                    $well_col = "C";
                    $well_id_no = $well_no - 24;
                break;
                case $well_no > 36 && $well_no < 49:
                    $well_col = "D";
                    $well_id_no = $well_no - 36;
                break;
                case $well_no > 48 && $well_no < 61:
                    $well_col = "E";
                    $well_id_no = $well_no - 48;
                break;
                case $well_no > 60 && $well_no < 73:
                    $well_col = "F";
                    $well_id_no = $well_no - 60;
                break;
                case $well_no > 72 && $well_no < 85:
                    $well_col = "G";
                    $well_id_no = $well_no - 72;
                break;
                case $well_no > 84 && $well_no < 97:
                    $well_col = "H";
                    $well_id_no = $well_no - 84;
                break;
            }

            $well = array(
              "date"=>$date,
              "well_pos"=>$well_col.$well_id_no,
              "tech"=>$tech,
              "plate_id"=>$plate,
              "row"=>$well_col,
              "col"=>$well_id_no,
            );

            create_well_record($well,$db);

            $wells[] = $well;
            $well_ct--;
            $well_no++;
        }
        return $wells;
    }

    function plate_has_wells($plate_id,$db){
        $plate_qry = "SELECT id FROM platerecordprocessing WHERE plateID='{$plate_id}'";
        $run = $db->query($plate_qry);

        if($run->num_rows > 0){
            return true;
        }else{
            return false;
        }
    }

    function fetch_plate_orders($plate_id,$db){
        $plate_qry = "SELECT DISTINCT OrderNumber FROM platerecordprocessing WHERE plateID = '{$plate_id}' AND OrderNumber IS NOT NULL ORDER BY id ASC";

        $run = $db->query($plate_qry);
        $orders = array();

        if($run->num_rows > 0){
            while($order = $run->fetch_array(MYSQLI_ASSOC)){
                $orders[] = $order["OrderNumber"];
            }
            return $orders;
        }else{
            return $orders;
        }

    }
    function fetch_plate_highlights($plate_id,$db){
        $plate_orders = fetch_plate_orders($plate_id,$db);
        $highlights = array();
        if(count($plate_orders) < 1){
            return false;
        }
        foreach($plate_orders as $order_no){
            $highlights[] = gen_order_highlight($order_no);
        }
        $highlight_css = implode("",$highlights);

        return $highlight_css;
    }

    function plate_validated($plate_id,$db){
        $plate_qry = "SELECT OrderNumber,isValid FROM platerecordprocessing WHERE plateID = '{$plate_id}'";
        $run = $db->query($plate_qry);

        if($run){
            if($run->num_rows > 0){
                return true;
            }else{
                return false;
            }
        }

    }

    function free_plate_wells($plate_id,$db){
        $plate_qry = "SELECT 96-(SELECT COUNT(*) FROM platerecordprocessing WHERE plateID='{$plate_id}' AND SampleID > 0) AS 'free_wells'";

        $run = $db->query($plate_qry);
        if($db->errno > 0){
            return null;
        }

        $result = $run->fetch_array(MYSQLI_ASSOC);
        return $result['free_wells'];
    }
    ////////////////////////////////
    //Well Methods
    ///////////////////////////////

    //fetch all wells for plate
    function fetch_plate_wells($plate_id,$plate,$db,$long = false){
        $plate_qry = "SELECT * FROM platerecordprocessing WHERE plateID='{$plate_id}' ORDER BY id ASC";
        $run = $db->query($plate_qry);

        if($run->num_rows > 0){
            $wells = array();
            $cols = array();
            $well_ct = 0;

            while($well = $run->fetch_array(MYSQLI_ASSOC)){

                if($well['isWellSkipped'] =="yes"){
                    $display_well = array(
                        "id"=>$well["id"],
                        "date"=>$well["date"],
                        "well_pos"=>$well["wellPos"],
                        "order_no"=>($well["Standard"] == "yes")?$well["Primer"]:$well["OrderNumber"],
                        "is_standard"=>($well["Standard"] == "yes")?true:false,
                        "plate"=>$well["plateID"],
                        "primer"=>'none',
                        "technician"=>$well["Technician"],
                        "is_skipped"=>true
                    );
                }else{
                    $display_well = array(
                        "id"=>$well["id"],
                        "date"=>$well["date"],
                        "well_pos"=>$well["wellPos"],
                        "well_no"=>$well_ct,
                        "service_date"=>date("Y-m-d"),
                        "designator"=>( $long == false)?fetch_designator($well["OrderNumber"],$db):"",
                        "plate_designator"=>( $long == false)?fetch_designator($well["OrderNumber"],$db):"",
                        "plate"=>$well["plateID"],
                        "order"=>($well["Standard"] == "yes")?$well["Primer"]:$well["OrderNumber"],
                        "is_standard"=>($well["Standard"] == "yes")?true:false,
                        "primer"=>(strlen($well["Primer"]) > 13 && $long == false)?substr($well["Primer"],0,10)."...":$well["Primer"],
                        "sample_name"=>$well["SampleName"],
                        "sample_id"=>$well["SampleID"],
                        "technician"=>$well["Technician"]
                    );
                }
                $wells[] = $display_well;

                if(!isset($cols[($well_ct % 12)])){
                    $cols[($well_ct % 12)] = array();
                }
                $cols[($well_ct % 12)][] = array(
                    "id"=>$well_ct
                );

                $well_ct ++;
            }

            foreach($cols as $col){
                $col_ct = 0;
                foreach($col as $well_data){
                    $well = $wells[$well_data["id"]];

                    @$well["row"] = $well["well_pos"][0];
                    @$well["col"] = $well["well_pos"][1].$well["well_pos"][2];

                    $wells[$well_data["id"]] = $well;

                    $col_ct++;
                }
            }

            return $wells;
        }else{
            return false;
        }

    }

    function fetch_well($well_id,$db){
        $well_qry = "SELECT * FROM platerecordprocessing WHERE id={$well_id}";

        $run = $db->query($well_qry);

        if($run->num_rows > 0){
            return $run->fetch_array(MYSQLI_ASSOC);
        }else{
            return array();
        }
    }

    //get well ids for range
    function get_well_range($first_well,$well_count){

        $plate = $first_well["plate"];
        $ittr = 0;
        $last_well_pos = $first_well['wellpos'];

        $row_range = range("A","H");
        $col_range = range(1,12);

        $row = $first_well['wellpos'][0];
        $col = substr($first_well['wellpos'],1,2);

        $wells[] = $row.$col;
        while($ittr < $well_count){
            $next_row = array_search($row,$row_range) +1;
            if($next_row == 8){
                $next_row = 0;
                $row = $row_range[0];
                $col++;
            }else{
                $row = $row_range[$next_row];
            }

            $wells[] = $row.$col;
            $ittr++;
        }

        return $wells;
    }

    //gen well order designator based on branch and order on plate
    function well_designator($plate_id,$order_no,$db){

        $branch = $_SESSION["branch"];

        //FIGURE OUT 1ST DESIGNATOR FOR PLATE
        $plate_bits = explode("-",$plate_id);

        $plate_designator = substr($plate_bits[2],2);

        $alpha_designator = ((int)$plate_designator < 1);


        $first_designator = "{$plate_designator}A";
        $alphabet = range('A', 'Z');

        $orders = fetch_plate_orders($plate_id,$db);

        $step = 0;
        if(count($orders) > 1){
            foreach($orders as $order){
                if($order_no == $order){
                    break;
                }
                $step++;

            }
        }
    	$designator = $first_designator;

        $order_designator = substr($first_designator,-1,1);

        if($alpha_designator){
        	$range = range('A', 'Z');
        	$plate_designator = substr($designator,0,1);
        }else{
            $range = range(10, 999);
            $plate_designator = substr($designator,0,3);
        }

        $next_step = array_search($order_designator, $alphabet) + $step;

    	if($next_step == 26){
    		$next_step = $next_step - 26;
            if($alpha_designator){
                $plate_designator = array_search($plate_designator, $range) + 1;
            }else{
                $plate_designator++;
            }
    	}

    	$new_designator = $plate_designator.$alphabet[$next_step];
        return $new_designator;
    }

    //fetch well designator for order
    function fetch_designator($order_no,$db){
        $well_qry = "SELECT PlateRec FROM ordertable WHERE OrderNumber={$order_no}";

        $run = $db->query($well_qry);

        if($run){
            if($run->num_rows > 0){
                $res = $run->fetch_array(MYSQLI_ASSOC);
                return $res["PlateRec"];
            }
        }

        return false;
    }

    //DEPRECIATED render well popup form | fill with well data, primer list
    function render_well_form($well_id,$db,$primer_list){
        $well_template = fetch_resource(_VIEWDIR."well.view");
        $well = fetch_well($well_id,$db);
        if($well == false){
            return false;
        }
        $render_data = array(
            "view"=>$well_template,
            "primer_list"=>$primer_list,
            "id"=>$well["id"],
            "date"=>$well["date"],
            "service_date"=>date("Y-m-d"),
            "well_pos"=>$well["wellPos"],
            "designator"=>fetch_designator($well["OrderNumber"],$db),
            "skipped"=>$well['isWellSkipped'] == "yes"?true:false,
            "plate"=>$well["plateID"],
            "order"=>($well["Standard"] == "yes")?$well["Primer"]:$well["OrderNumber"],
            "is_standard"=>($well["Standard"] == "yes")?true:false,
            "primer"=>$well["Primer"],
            "sample_name"=>$well["SampleName"],
            "sample_id"=>$well["SampleID"],
            "technician"=>$well["Technician"]
        );

        $rendered = render_view($render_data);

        return $rendered;
    }

    //create well
    function create_well_record($well_data,$db){
        $well_qry = "INSERT INTO platerecordprocessing SET ".
                    "date='{$well_data['date']}', plateID='{$well_data['plate_id']}', Technician='{$well_data['tech']}', wellPos='{$well_data['well_pos']}'";

        $run = $db->query($well_qry);

        if($db->affected_rows > 0){
            return true;
        }else{
            return false;
        }
    }

    //
    //well update functions
    //

    function set_well_standard($well_id,$std,$db){
        //update data for specific well
        $tech = $_SESSION["prp_tech"];
        //insert data into platerecordprocessing
        $well_qry = "UPDATE platerecordprocessing SET OrderNumber=NULL, Primer='{$std}', Standard='yes', Technician='{$tech}'  WHERE id='{$well_id}'";

        $run = $db->query($well_qry);

        if($db->affected_rows < 1){
            return false;
        }

        if($db->errno > 0){
            $err = true;
            $error_data[] = array(
                "qry"=>$well_qry,
                "db"=>$db,
                "run"=>$run
            );
            return array(
                $err,$error_data
            );
        }

        return array(
            "ok"=>true
        );
    }

    function skip_well($well_id,$db){
        $well = fetch_well($well_id,$db);
        //skip well

        //update platerecordprocessing
        $skip_qry = "UPDATE platerecordprocessing SET isWellSkipped='yes' WHERE id={$well_id}";
        $run = $db->query($skip_qry);

        if($db->errno > 0){
            $error =  array(
                "error"=>true,
                "msg"=>"Error skipping well [{$db->error}]"
            );

            return $error;
        }

        return array("ok"=>true);
    }

    //well deletion methods
    function delete_well($well_id,$db){
        //delete well
        $well = fetch_well($well_id,$db);
        //skip well

        $skip_qry = "UPDATE platerecordprocessing SET isWellSkipped='yes' WHERE id={$well_id}";
        $run = $db->query($skip_qry);
        if($db->errno > 0){
            $error =  array(
                "error"=>true,
                "msg"=>"Error skipping well [{$db->error}]"
            );

            output_json($error);
        }
        //update sampletable
        $skip_qry = "UPDATE sampletable SET isWellSkipped='yes', isDisabled='yes' WHERE OrderNumber='{$well['OrderNumber']}' AND plateID='{$well['plateID']}'";
        $run = $db->query($skip_qry);

        if($db->errno > 0){
            return false;
        }

        //reset well for usage
        remove_sample($well,$db);
    }

    //Get next well
    function get_next_well($current_well){
        $alphabet = range('A', 'Z');

        if($current_well == "H12"){
            return false;
        }

        $alpha = substr($current_well,0,1);
        if(strlen($current_well) == 3){
            $no = substr($current_well,1,2);
        }else{
            $no = substr($current_well,1,1);
        }

        if($alpha == "H"){
            $next_no = $no + 1;
            $next_alpha = "A";
        }else{
            $next_no = $no;
            $next_alpha = $alphabet[array_search($alpha,$alphabet) + 1];
        }

        $next_well = $next_alpha.$next_no;

        return $next_well;

    }

    //Get prev well
    function get_prev_well($current_well){
        $alphabet = range('A', 'Z');

        if($current_well == "A1"){
            return false;
        }

        $alpha = substr($current_well,0,1);
        if(strlen($current_well) == 3){
            $no = substr($current_well,1,2);
        }else{
            $no = substr($current_well,1,1);
        }

        if($alpha == "A"){
            $prev_no = $no - 1;
            $prev_alpha = "H";
        }else{
            $prev_no = $no;
            $prev_alpha = $alphabet[array_search($alpha,$alphabet) - 1];
        }

        $prev_well = $prev_alpha.$prev_no;

        return $prev_well;
    }

    //Check if well is empty
    function is_well_empty($plate_id,$well,$db){
        $well_qry  = "SELECT * FROM platerecordprocessing WHERE wellPos='{$well}' AND plateID='{$plate_id}' AND isWellSkipped='no' AND OrderNumber IS NULL AND Primer IS NULL";

        $run = $db->query($well_qry);

        if($run->num_rows < 1){
            return false;
        }else{
            return true;
        }
    }

    ////////////////////////////////
    //Sample Methods
    ///////////////////////////////

    //fetch all samples from sample table for
    function fetch_order_samples($order_no,$plate_id,$db){

        $free_wells = (int)free_plate_wells($plate_id,$db);
        $limit_qry = "LIMIT {$free_wells}";
        $order_qry  = "SELECT SampleName,SampleID,Primer,OrderNumber FROM sampletable WHERE OrderNumber={$order_no} AND isWellSkipped IS NULL AND isDisabled='no' AND plateID IS NULL {$limit_qry}";
        $wells = array();

        $run = $db->query($order_qry);
        if($db->error){
            $error = array(
                "error"=>true,
                "msg"=>"Error fetching samples for order #{$order_no}",
                "order"=>$order_no,
                "plate"=>$plate_id
            );
            output_json($error);
        }
        if($run->num_rows > 0){
            while($well = $run->fetch_array(MYSQLI_ASSOC)){
                $wells[] = $well;
            }
        }else{
            $error = array(
                "error"=>true,
                "msg"=>"No unused samples found for order# {$order_no}",
                "order"=>$order_no,
                "plate"=>$plate_id
            );
            output_json($error);
        }
        return $wells;
    }

    function fetch_sample($order_no,$sample_id,$db){
        $sample_qry  = "SELECT SampleName,SampleID,Primer,OrderNumber FROM sampletable WHERE OrderNumber='{$order_no}' AND SampleID='{$sample_id}'";

        $run = $db->query($sample_qry);
        if($db->error){
            $error = array(
                "error"=>true,
                "msg"=>"Error fetching sample id# {$sample_id} (order# {$order_no})",
                "order"=>$order_no,
                "sampleID"=>$sample_id
            );
            output_json($error);
        }

        if($run->num_rows < 1){
            $error = array(
                "error"=>true,
                "msg"=>"No sample found for sample id# {$sample_id} (order# {$order_no})",
                "order"=>$order_no,
                "plate"=>$plate_id
            );
            output_json($error);
        }

        $sample = $run->fetch_array(MYSQLI_ASSOC);

        return $sample;
    }

    //insert sample data into well records
    function samples_to_wells($samples,$wells,$plate_id,$order_no,$service_date,$db){
        $err =  false;
        //get plate number
        $plate_no = get_plate_no($plate_id,$order_no,$db);
        $tech = $_SESSION['prp_tech'];

        foreach($samples as $sid=>$sample){
            $well = $wells[$sid];
            //insert data into platerecordprocessing
            $well_qry = "UPDATE platerecordprocessing SET ".
                        "SampleName='{$sample["SampleName"]}', SampleID='{$sample["SampleID"]}', ".
                        "Primer='{$sample["Primer"]}', OrderNumber='{$sample["OrderNumber"]}', Technician='{$tech}' ".
                        "WHERE wellPos='{$well}' AND plateID='{$plate_id}' AND OrderNumber IS NULL AND Standard='no' AND isWellSkipped='no'";


            $run = $db->query($well_qry);
            if($db->affected_rows < 1){
                return false;
            }
            if($db->errno > 0){
                $error = array(
                    "error"=>true,
                    "msg"=>"Plate update failed",
                    "location"=>"Sample to Wells Loop, prp update"
                );
                output_json($error);

            }

            //update sample table
            $sample_qry  = "UPDATE sampletable SET plateID='{$plate_id}', plateNum='{$plate_no}', wellPos='{$well}' ".
                           "WHERE SampleID='{$sample['SampleID']}' AND OrderNumber='{$sample['OrderNumber']}'";
            $run = $db->query($sample_qry);

            if($db->errno > 0){
                $error = array(
                    "error"=>true,
                    "msg"=>"Plate update failed",
                    "location"=>"Sample to Wells Loop, sample update"
                );
                output_json($error);
            }

            $plate_designator = well_designator($plate_id,$order_no,$db);
            $designator_qry  = "UPDATE ordertable SET PlateRec='{$plate_designator}' WHERE OrderNumber='{$sample['OrderNumber']}'";
            $run = $db->query($designator_qry);
            if($db->errno > 0){
                $error = array(
                    "error"=>true,
                    "msg"=>"Plate update failed",
                    "location"=>"Sample to Wells Loop, order update"
                );
                output_json($error);
            }

            if(order_has_service($sample["OrderNumber"],$service_date,$plate_designator,$plate_no,$db)){
                $services_qry  = "UPDATE servicestable SET plateRec='{$plate_designator}' WHERE ServiceDate='{$service_date}' AND OrderNumber='{$sample["OrderNumber"]}' AND plateNumber={$plate_no}";
                $run = $db->query($services_qry);

                if($db->errno > 0){
                    $error = array(
                        "error"=>true,
                        "msg"=>"Plate update failed",
                        "location"=>"Sample to Wells Loop, service update"
                    );
                    output_json($error);
                }
            }else{
                create_order_service($sample["OrderNumber"],$plate_designator,$plate_no,$service_date,$db);
            }
        }
        return true;
    }

    //update sample ids in platerecordprocessing & sampletable
    function update_sample_id($well_data,$new_id,$db){
        //update well sample id and update all subsequent sample ids
        $diff = (int)($new_id - $well_data["SampleID"]);

        $update_sample_qry = "UPDATE sampletable SET SampleID=(SampleID + {$diff}) ".
                      "WHERE OrderNumber={$well_data["OrderNumber"]} AND SampleID >= {$well_data["SampleID"]} ORDER BY SampleID DESC";

        // die($update_sample_qry);
        $run = $db->query($update_sample_qry);

        if($db->errno > 0){
            $err = true;
            $error_data[] = array(
                "qry"=>$update_sample_qry,
                "db"=>$db,
                "run"=>$run
            );
            return array(
                $err,$error_data
            );
        }

        $update_well_qry = "UPDATE platerecordprocessing SET SampleID=(SampleID + {$diff}) ".
                      "WHERE SampleID >= {$well_data["SampleID"]} AND OrderNumber='{$well_data["OrderNumber"]}' ORDER BY SampleID DESC";
        $run = $db->query($update_well_qry);

        if($db->errno > 0){
              $err = true;
              $error_data[] = array(
                  "qry"=>$update_well_qry,
                  "db"=>$db,
                  "run"=>$run
              );
              return array(
                  $err,$error_data
              );
        }

        return true;
    }

    //update sample name in platerecordprocessing & sampletable
    function update_sample_name($well_data,$db){

        $update_sample_qry = "UPDATE sampletable SET SampleName='{$well_data["samplename"]}' ".
                             "WHERE OrderNumber={$well_data["orderno"]} AND SampleID = {$well_data["sampleid"]}";

        // die($update_sample_qry);
        $run = $db->query($update_sample_qry);

        if($db->errno > 0){
            $err = true;
            $error_data[] = array(
                "qry"=>$update_sample_qry,
                "db"=>$db,
                "run"=>$run
            );
            return array(
                $err,$error_data
            );
        }

        $update_well_qry = "UPDATE platerecordprocessing SET SampleName='{$well_data["samplename"]}' ".
                           "WHERE id = {$well_data["id"]}";
        $run = $db->query($update_well_qry);

        if($db->errno > 0){
              $err = true;
              $error_data[] = array(
                  "qry"=>$update_well_qry,
                  "db"=>$db,
                  "run"=>$run
              );
              return array(
                  $err,$error_data
              );
        }

        return true;
    }

    //update sample primer in platerecordprocessing & sampletable
    function update_sample_primer($well_data,$db){

        $update_sample_qry = "UPDATE sampletable SET Primer='{$well_data["primer"]}' ".
                             "WHERE OrderNumber={$well_data["orderno"]} AND SampleID = {$well_data["sampleid"]}";

        // die($update_sample_qry);
        $run = $db->query($update_sample_qry);

        if($db->errno > 0){
            $err = true;
            $error_data[] = array(
                "qry"=>$update_sample_qry,
                "db"=>$db,
                "run"=>$run
            );
            return array(
                $err,$error_data
            );
        }

        $update_well_qry = "UPDATE platerecordprocessing SET Primer='{$well_data["primer"]}' ".
                           "WHERE id = {$well_data["id"]}";
        $run = $db->query($update_well_qry);

        if($db->errno > 0){
              $err = true;
              $error_data[] = array(
                  "qry"=>$update_well_qry,
                  "db"=>$db,
                  "run"=>$run
              );
              return array(
                  $err,$error_data
              );
        }

        return true;
    }

    //check if order number has any samples in sampletable
    function order_has_samples($order_no,$db){
        $order_qry  = "SELECT SampleName FROM sampletable WHERE OrderNumber={$order_no}";
        $run = $db->query($order_qry);
        if($run->num_rows > 0){
            return true;
        }

        return false;
    }

    //remove sample from well
    function remove_sample($well,$db){
            $well_data = fetch_well($well,$db);

            if((int)$well_data['OrderNumber'] < 1 && $well_data['Standard'] == 'no' && is_null($well_data['isWellSkipped'])){
                return true;
            }

            $tech = $_SESSION["prp_tech"];

            $update_sample_qry = "UPDATE sampletable SET plateID=NULL, plateNum=NULL, wellPos=NULL, isWellSkipped=NULL, isDisabled='no' ".
                                 "WHERE OrderNumber='{$well_data["OrderNumber"]}' AND SampleID ='{$well_data["SampleID"]}'";

            $run = $db->query($update_sample_qry);

            if($db->errno > 0){
                $error_data = array(
                    "error"=>true,
                    "title"=>"Sample Removal Error",
                    "text"=>"There was an error removing the sample from the well. [remove_sample()|sampletable:{$db->error}]"
                );

                output_json($error_data);
                return false;
            }

            $update_well_qry = "UPDATE platerecordprocessing SET ".
                        "SampleName=NULL, SampleID=NULL, Standard='no', ".
                        "Primer=NULL, OrderNumber=NULL, Technician='{$tech}', isWellSkipped='no' ".
                        "WHERE wellPos='{$well_data["wellPos"]}' AND plateID='{$well_data["plateID"]}'";

            $run = $db->query($update_well_qry);

            if($db->errno > 0){
                $err = true;
                $error_data = array(
                    "error"=>true,
                    "title"=>"Sample Removal Error",
                    "text"=>"There was an error removing the sample from the well. [remove_sample()|prp:{$db->error}]"
                );

                output_json($error_data);

                return false;
            }

            return true;
    }
    function reset_plate_samples($plate_id,$db){
        $reset_qry = "UPDATE sampletable SET plateID=NULL, plateNum=NULL, wellPos=NULL, isWellSkipped=NULL, isDisabled='no' WHERE plateID='{$plate_id}'";

        $run = $db->query($reset_qry);

        if($db->errno > 0){
            $error_data = array(
                "error"=>true,
                "title"=>"Sample Removal Error",
                "text"=>"There was an error removing the sample from the plate. [remove_sample()|sampletable:{$db->error}]"
            );

            output_json($error_data);
        }else{
            return true;
        }
    }
    function shift_sample($plate_id,$well_sample,$to_well,$db){
        if($well_sample["is_standard"] == 0){
            $samples = array(
                fetch_sample($well_sample['orderno'],$well_sample['sampleid'],$db)
            );
            $wells = array(
                $to_well
            );
            $service_date = date("Y-m-d");
            $order_no = $well_sample["orderno"];

            samples_to_wells($samples,$wells,$plate_id,$order_no,$service_date,$db);
        }else{
            //update data for specific well
            $tech = $_SESSION["prp_tech"];
            $std = $well_sample["primer"];
            //insert data into platerecordprocessing
            $well_qry = "UPDATE platerecordprocessing SET OrderNumber=NULL, Primer='{$std}', Standard='yes', Technician='{$tech}'  WHERE plateID='{$plate_id}' AND wellPos='{$to_well}'";

            $run = $db->query($well_qry);

            if($db->affected_rows < 1){
                $error = array(
                    "error"=>true,
                    "title"=>"Sample Shift Error",
                    "msg"=>"Sample shift failed, standard not set to desistination well"
                );
                output_json($error);
            }
        }
        remove_sample($well_sample['id'],$db);

        return true;
    }

    ////////////////////////////////
    //Validation Methods
    ///////////////////////////////
    function plate_validation($plate_id,$db){
        //get orders
        $orders = fetch_plate_orders($plate_id,$db);
        $validation_data = array();

        if(count($orders) < 1){
            return false;
                // $error = array(
                //     "error"=>true,
                //     "msg"=>"Plate update failed",
                //     "location"=>"Sample to Wells Loop, service update"
                // );
                // output_json($error);
        }

        foreach($orders as $id=>$order){
            //get designators
            $designator = fetch_designator($order,$db);
            $order_row = array(
                "order_no"=>$order,
                "designator"=>$designator
            );
            $validation_data[] = $order_row;
        }

        return $validation_data;
    }
    function gen_plate_file($plate_id,$db){
        @session_start();
        $file_template = fetch_resource(_VIEWDIR."file_template.view");
        //grab all wells for plate
        $wells = fetch_plate_wells($plate_id,$db,true);

        foreach($wells as $id=>$well){
            $wells[$id]["sample_name"] = (strlen($wells[$id]["sample_name"]) > 0)?$wells[$id]["sample_name"]:"NONE";
            $plate_no = get_plate_no($plate_id,$wells[$id]["order"],$db);
            validate_services($wells[$id]["id"],$wells[$id]["date"],$wells[$id]["order"],$plate_no,$db);
        }

        //build data for file
        $txt_data = array(
            "wells"=>$wells,
            "owner"=>$_SESSION["prp_tech"],
            "operator"=>$_SESSION["prp_tech"],
            "container_name"=>$plate_id,
            "plateid"=>$plate_id,
            "comment"=>"  ",
            "description"=>_PRP_TEXT_DESCRIPTION,
            "plate_sealing"=>_PRP_TEXT_PLATESEALING,
            "app_type"=>"  ",
            "container_type"=>_PRP_TEXT_CONTAINERTYPE,
            "scheduling_pref"=>_PRP_TEXT_SCHEDULINGPREF,
            "results_group1"=>_PRP_TEXT_RESULTSGROUP1,
            "instrument_protocol"=>_PRP_TEXT_INSTRUMENTPROTOCOL,
            "analysis_protocol"=>_PRP_TEXT_ANALYSISPROTOCOL
        );

        $plate = array(
            "view"=>$file_template,
            "data"=>$txt_data
        );

        return $plate;
        //write data to file
    }
    function write_plate_file($plate_file,$plate_id){
        $new_file = _PRP_FILEARCH."{$plate_id}.xls";
        $write = file_put_contents($new_file,$plate_file);
        if(file_exists($new_file)){
            return $new_file;
        }else{
            return false;
        }
    }
    function validate_services($well_id,$date,$order,$plate_no,$db){
        $designator = fetch_designator($order,$db);
        $service_date = explode(" ",$date)[0];
        if((int)$order < 1){
            return false;
        }
        $services_qry  = "UPDATE servicestable SET isValid=1 WHERE OrderNumber='{$order}' AND ServiceDate='{$service_date}' AND PlateRec='{$designator}' AND plateNumber={$plate_no}";

        $run = $db->query($services_qry);

        if($db->affected_rows > 0){

            $well_qry  = "UPDATE platerecordprocessing SET isProcessed=1 WHERE id='{$well_id}'";

            $run = $db->query($well_qry);

            if($db->affected_rows > 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;

        }
    }

    ////////////////////////////////
    //Output Methods
    ///////////////////////////////
    function output_file($file,$file_name,$type = "application/vnd.ms-excel",$ext = "xls"){
        $fileName   = "{$file_name}.{$ext}";

        header("Content-Type: {$type}");
        header("Content-Transfer-Encoding: Binary");
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        // Run this until we have read the whole file.
        if(file_exists($file)){
            $file_data = file_get_contents($file);

            exit($file_data);
        }else{
            exit("File not found T_T");
        }
    }

    function output_json($data){
        $json = json_encode($data);

        header("Content-Type:application/json");
        exit($json);
    }
