<?php
    session_start();

    $action = $_GET["action"];
    $actions = array(
        "get","mod","del","dl","clear","reset"
    );
    if(!in_array($action,$actions)){
        header("HTTP/1.0 404 Not Found");
        die();
    }
    require_once("../prp-config.php");

    //dependencies
    $prp_dependencies = array(
        _BINDIR."func.php",
        _BINDIR."lib/vendor/autoload.php" /*Composer Autoloader*/
    );

    //load dependencies
    foreach ($prp_dependencies as $key => $dep) {
      require_once($prp_dependencies[$key]);
    }

    //connect to db
    $db = connect_db($db_config);


    switch ($action) {
        case 'get':
            $well_id = (int)$_GET["well"];
            header("Content-Type:text/html");
            echo render_well_form($well_id,$db,$primer_list);
        break;
        case 'mod':
            $sub_action = $_GET["subAction"];
            $well_data = $_GET["welldata"];

            switch($sub_action){
                default:
                    //pull current version of well
                    $old_well = fetch_well($well_data["id"],$db);


                    //check if Standard
                    $is_standard = (strtoupper(substr($well_data['orderno'],0,2)) == "ST")?true:false;
                    if( $is_standard == true){
                        output_json(set_well_standard($well_data["id"],$well_data["orderno"],$db));
                    }

                    //order number change to update wells and data in sampletable
                    if($well_data["orderno"] != $old_well["OrderNumber"]){

                        if(!is_well_empty($welldata["plate"],$welldata["wellpos"],$db)){
                            remove_sample($well_data["id"],$db);
                        }

                        //get all samples for order
                        $samples = fetch_order_samples($well_data["orderno"],$well_data["plate"],$db);

                        if(count($samples) > 0){
                            //get wells
                            $wells = get_well_range($well_data,count($samples) - 1);
                            //insert samples into wells
                            samples_to_wells($samples,$wells,$well_data["plate"],$well_data["orderno"],$well_data['servicedate'],$db);
                        }else{
                            $error = array(
                                "msg"=>"No unused samples found for order# {$well_data["orderno"]}",
                                "order"=>$well_data["orderno"],
                                "plate"=>$well_data["plate"]
                            );
                            output_json($error);
                        }
                    }

                    //check for sampleID change, to update other applicable sampleIDs
                    // if(isset($well_data["sampleid"]) && $well_data["sampleid"] != $old_well["SampleID"]){
                    //     update_sample_id($old_well,$well_data["sampleid"],$db);
                    // }

                    //check for samplename change, to update other applicable SampleNames
                    if(isset($well_data["samplename"]) && $well_data["samplename"] != $old_well["SampleName"]){
                        update_sample_name($well_data,$db);
                    }

                    //check for primer change, to update other applicable samples
                    if(isset($well_data["primer"]) && $well_data["primer"] != $old_well["Primer"]){
                        update_sample_primer($well_data,$db);
                    }

                    //check for sampleID change, to update other applicable sampleIDs
                    if(isset($well_data["designator"]) && $well_data["designator"] != fetch_designator($old_well["OrderNumber"],$db)){
                        update_order_designator($well_data["designator"],$well_data["orderno"],$well_data["servicedate"],$db);
                    }
                break;
                case "shift":
                    $direction = $_GET["shift"];
                    $well = $_GET["well"];
                    $plate_id = $well_data["plate"];

                    if($direction == "u"){
                        //shift well up
                        $shift_to = get_prev_well($well);
                    }
                    if($direction == "d"){
                        //shifty well down
                        $shift_to = get_next_well($well);
                    }

                    if(is_well_empty($plate_id,$shift_to,$db)){
                        //move sample, return ok
                        shift_sample($plate_id,$well_data,$shift_to,$db);
                    }else{
                        //return error
                        $error = array(
                            "error"=>true,
                            "title"=>"Sample Shift Error",
                            "msg"=>"Sample shift failed, destination well ({$shift_to}) is not empty."
                        );
                        output_json($error);
                    }
                break;
                case "skip":
                    $well = $_GET["well"];

                    output_json(skip_well($well,$db));
                break;
            }
        break;
        case 'shift':
            //moving wells around
            switch($sub_action ){

            }
        break;
        case 'clear':
            $well_id = (int)$_GET["well"];
            $well = fetch_well($well_id,$db);
            if((int)$well['OrderNumber'] > 0 && strlen($well['Primer']) > 0 || $well['Standard'] == "yes" || $well["isWellSkipped"] == 'yes'){
                if(remove_sample($well_id,$db)){
                    $output = array(
                        "ok"=>true
                    );
                    output_json($output);
                }
            }else{
                $output = array(
                    "error"=>true,
                    "title"=>"Reset Well Error",
                    "msg"=>"Well already empty"
                );
                output_json($output);
            }
        break;
        case 'del':
            $well_id = (int)$_GET["well"];
            $well = fetch_well($well_id,$db);

            if((int)$well['OrderNumber'] > 0 && strlen($well['Primer']) > 0){
                output_json(skip_well($well_id,$db));
            }else{
                output_json(array(
                    "error"=>true
                ));
            }
        break;
        case 'reset':
            //reset all plate wells and samples

            $plate_id = sanitize_str($_GET["plateID"],$db);
            //grab all wells for plate
            $wells = fetch_plate_wells($plate_id,substr($plate_id,-1,1),$db,$long = false);

            //loop through wells
            foreach($wells as $well){
                //remove sample/reset well
                remove_sample($well['id'],$db);
            }
            reset_plate_samples($plate_id,$db);

            $output = array("ok"=>true);
            output_json($output);
        break;
        case 'dl':
            $plate_id = sanitize_str($_GET["plateid"],$db);

            if(plate_validated($plate_id,$db)){
                $plate_file_path = _PRP_FILEARCH."{$plate_id}.txt";
            }else{
                $plate = gen_plate_file($plate_id,$db);
                $plate_file = render_view($plate);
                $plate_file_path = write_plate_file($plate_file,$plate_id);
            }

            header("Content-Type: text/plain");
            output_file($plate_file_path,$plate_id);
        break;
    }
?>
