<?php

////////////////////////////////
//Plate Record Processing App
//Config file
///////////////////////////////

    ////////////////////////////////
    //Config
    ///////////////////////////////

    //directories
    define("_PRPDIR",__DIR__);
    define("_BINDIR",_PRPDIR."/bin/");
    define("_CSSDIR",_PRPDIR."/ui/css/");
    define("_VIEWDIR",_PRPDIR."/ui/views/");
    define("_JSDIR",_PRPDIR."/ui/js/");
    define("_UILIBDIR",_PRPDIR."/ui/lib/");
    define("_JSIDIR","./_prp/jsi/"); //php|javascript interface
    define("_WEBVIEWDIR","./_prp/ui/views/");
    define("_PRP_FILEARCH",_PRPDIR."/file_archive/");

    //db config
    $db_config = array(
        "host"=>"192.168.2.105",
        "usr"=>"dev",
        "pwd"=>'t3h@bd3v!',
        "db"=>"etonbioscience",
        // "port"=> /**If using non standard mysql port**/
    );

    //primer list
    $primer_list = array(
        "T7",
        "T7t",
        "T3",
        "SP6",
        "M13F",
        "M13F-40",
        "M13R",
        "AOX1-F",
        "AOX1-R",
        "pGEX-F",
        "pGEX-R",
        "BGH-R",
        "GFP-F",
        "GFP-R",
        "GAG",
        "GAG-R",
        "CYC1-R",
        "pFastBac-F",
        "pFastBac-R",
        "pBAD-F",
        "pBAD-R",
        "CMV-F",
        "LKO.1 5"
    );

    //text file constants
    define("_PRP_TEXT_DESCRIPTION","96-Well"); //description in text file
    define("_PRP_TEXT_PLATESEALING","Septa"); //platesealing in text file
    define("_PRP_TEXT_CONTAINERTYPE","Regular"); //container type in text file
    define("_PRP_TEXT_SCHEDULINGPREF","1234"); //schedulingpref in text file
    define("_PRP_TEXT_RESULTSGROUP1","BDX-results"); //Results Group 1 in text file
    define("_PRP_TEXT_INSTRUMENTPROTOCOL","Eton_BDX_longseq50-POP7"); //Instrument Protocol 1 in text file
    define("_PRP_TEXT_ANALYSISPROTOCOL","SeqAnalysis"); //Analysis Protocol 1 in text file
