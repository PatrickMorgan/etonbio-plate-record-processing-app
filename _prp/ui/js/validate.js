_PRP.Validate = {
    init:function(){
        this.bind();
    },
    plateLink:function(){
        var plate = _PRP.Validate.plateID.substr(-1,1);
        var date = _PRP.Validate.plateID.split(plate)[0];

        return _PRP.config.loc+"?prp_view=plate&plate="+plate+"&date="+date+"&tech="+_PRP.Validate.techID;
    },
    bind:function(){
        var backBtn = document.getElementById('back2plate-btn'),
            validateBtn = document.getElementById('validate-btn');

            try{
                validateBtn.addEventListener('click',_PRP.Validate.DL);
            }catch(e){

            }
            backBtn.addEventListener('click',function(){
                window.location = _PRP.Validate.plateLink();
            });

    },
    DL:function(){
        var dlFrame = "<iframe style='display:none' src='"+_PRP.config.jsi+"?action=dl&plateid="+_PRP.Validate.plateID+"'></iframe>";
        var plate = _PRP.Validate.plateID.substr(-1,1);
        var date = _PRP.Validate.plateID.split(plate)[0];
        $jQ("#validate").append(dlFrame);
        // window.location = _PRP.Validate.plateLink();
    }
}

//fire init
_PRP.Validate.init();
