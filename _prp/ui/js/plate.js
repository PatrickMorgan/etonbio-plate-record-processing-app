_PRP.Plate = {
    init:function(){
        this.bind();
    },
    bind:function(){
        var wells = $jQ(".well-save-btn"),
            wellsUp = $jQ(".well-shift-up"),
            wellsDown = $jQ(".well-shift-down"),
            wellsDel = $jQ(".well-discard"),
            wellsReset = $jQ(".well-reset"),
            wellsSkip = $jQ(".well-skip"),
            validateBtn = document.getElementById('validate_btn'),
            resetBtn = document.getElementById('reset_btn');

            validateBtn.addEventListener('click',_PRP.Plate.validate);
            wells.each(function(){
                var well = $jQ(this);
                well.on("click",function(){
                    _PRP.Plate.well.save(well.data("id"));
                });
            });

            wellsUp.each(function(){
                var well = $jQ(this);
                well.on("click",function(){
                    _PRP.Plate.well.shift(well.data("id"),"u");
                });
            });
            wellsDown.each(function(){
                var well = $jQ(this);
                well.on("click",function(){
                    _PRP.Plate.well.shift(well.data("id"),"d");
                });
            });
            wellsReset.each(function(){
                var well = $jQ(this);
                well.on("click",function(){
                    _PRP.Plate.well.reset(well.data("id"));
                });
            });
            wellsDel.each(function(){
                var well = $jQ(this);
                well.on("click",function(){
                    _PRP.Plate.well.del(well.data("id"));
                });
            });
            wellsSkip.each(function(){
                var well = $jQ(this);
                well.on("click",function(){
                    _PRP.Plate.well.skip(well.data("id"));
                });
            });
    },
    reload:function(){
        swal({title:"Loading Well Changes",timer:750,text:"",showConfirmButton:false},function(){
            window.location = window.location;
        });
    },
    well:{
        fetch:function(wellID,cb){
            var template = $jQ.get(_PRP.config.jsi,{action:"get",well:wellID},function(data){
                if(typeof(cb) == "function"){
                    cb(data);
                }
            });
            return template;
        },
        shift:function(wellID,dir){
            var wellData = $jQ("#well-"+wellID).serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});
            console.log(wellData);
            var request = $jQ.get(_PRP.config.jsi,{action:"mod",subAction:"shift",welldata:wellData,well:wellData.wellpos,shift:dir},function(data){
                if(data.error){
                    swal({type:'error',title:data.title?data.title:'Update Error',text:data.msg});
                }else{
                    _PRP.Plate.reload();
                }
            });
            return request;
        },
        push:function(wellID,cb){
            var wellData = $jQ("#well-"+wellID).serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});
            console.log(wellData);
            var request = $jQ.get(_PRP.config.jsi,{action:"mod",welldata:wellData},function(data){
                console.log(data);
                if(data.error){
                    swal({type:'error',title:'Update Error',text:data.msg});
                }else{
                    if(typeof(cb) == "function"){
                        cb(data);
                    }
                }
            });
            return request;
        },
        open:function(wellID,wellPos){
            _PRP.Plate.well.fetch(wellID,function(template){
                swal({
                    title:null,
                    text:template,
                    html:true,
                    showCancelButton: true,
                    cancelButtonText:"Done",
                    confirmButtonText:"Update",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },_PRP.Plate.well.save);
            });
        },
        del:function(wellID){
            swal({
                title:"Confirm Delete?",
                text:"",
                html:true,
                showCancelButton: true,
                cancelButtonText:"Cancel",
                confirmButtonText:"Confirm & Delete",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },function(){
                $jQ.get(_PRP.config.jsi,{action:"del",well:wellID},function(data){
                    console.log(data);
                    if(data.error){
                        console.log(data);
                        swal({type:'error',title:data.title?data.title:'Well Skip Error',text:data.msg});
                    }else{
                        _PRP.Plate.reload();
                    }
                });
            },function(){
                _PRP.Plate.well.open(wellID);
            });
        },
        save:function(wellID){
            _PRP.Plate.well.push(wellID,function(data){
                swal({title:"Loading Well Changes",timer:750,text:"",showConfirmButton:false},function(){
                    _PRP.Plate.reload();
                });
            });
        },
        skip:function(wellID){
            swal({
                title:"Confirm Well Skip",
                text:"",
                html:true,
                showCancelButton: true,
                cancelButtonText:"Cancel",
                confirmButtonText:"Confirm & Skip",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },function(){
                $jQ.get(_PRP.config.jsi,{action:"mod",subAction:"skip",well:wellID},function(data){
                    if(data.error){
                        console.log(data);
                        swal({type:'error',title:data.title?data.title:'Well Skip Error',text:data.msg});
                    }else{
                        _PRP.Plate.reload();
                    }
                });
            });
        },
        reset:function(wellID){
            swal({
                title:"Confirm Plate Reset",
                text:"All assigned samples will be removed from this well plate.",
                html:true,
                showCancelButton: true,
                cancelButtonText:"Cancel",
                confirmButtonText:"Confirm & Reset",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },function(){
                $jQ.get(_PRP.config.jsi,{action:"clear",well:wellID},function(data){
                    if(data.error){
                        console.log(data);
                        swal({type:'error',title:data.title?data.title:'Plate Reset Error',text:data.msg});
                    }else{
                        _PRP.Plate.reload();
                    }
                });
            });
        }
    },
    reset:function(){
        swal({
            title:"Confirm Plate Reset",
            text:"All assigned samples will be removed from this well plate.",
            html:true,
            showCancelButton: true,
            cancelButtonText:"Cancel",
            confirmButtonText:"Confirm & Reset",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },function(){
            $jQ.get(_PRP.config.jsi,{action:"reset",plateID:_PRP.Plate.id},function(data){
                if(data.error){
                    console.log(data);
                    swal({type:'error',title:data.title?data.title:'Plate Reset Error',text:data.msg});
                }else{
                    _PRP.Plate.reload();
                }
            });
        });
    },
    validate:function(){
        window.location = _PRP.config.loc+"?prp_view=validate&plateid="+_PRP.Plate.id+"&techid="+_PRP.Plate.tech;
    }
}
//fire init
_PRP.Plate.init();
