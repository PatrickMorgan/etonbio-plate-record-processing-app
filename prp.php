<?php

//testing
@session_start();

$admin_branch = isset($_GET['branch'])?$_GET["branch"]:"SD"; //$_SESSION['admin_branch']
$_SESSION["branch"] = $admin_branch;
//testing

////////////////////////////////
//Plate Record Processing App
//Main file
///////////////////////////////

    require_once("_prp/prp-config.php");

    //dependencies
    $prp_dependencies = array(
        _BINDIR."func.php",
        _BINDIR."lib/vendor/autoload.php" /* Composer Autoloader for Mustache Template Engine */
    );

    //load dependencies
    foreach ($prp_dependencies as $key => $dep) {
      require_once($prp_dependencies[$key]);
    }

    //view ACL
    $view_acl = array(
        "init","plate","well","validate"
    );

    $view = isset($_GET['prp_view'])?$_GET['prp_view']:"init";

    if(!in_array($view,$view_acl)){
        die("NOT VALID");
    }

    $db = connect_db($db_config);

    ////////////////////////////////
    //View routing
    ///////////////////////////////
    switch($view){
        case "plate":
            $init_data = array(
                "plate"=>sanitize_str($_GET['plate'],$db),
                "date"=>sanitize_str($_GET['date'],$db),
                "tech"=>fetch_user((int)$_GET['tech'],$db),
                "tech_id"=>(int)$_GET['tech']
            );
            $plate_id = $init_data["date"].$init_data["plate"];

            @session_start();
            $_SESSION["prp_tech"] = $init_data["tech"];

            if(gettype($db) != "array"){
                $init_view = load_view("init",array(
                   "employees"=>fetch_user_list($admin_branch,$db),
                   "technician"=>$init_data["tech"],
                   "tech_id"=>$init_data["tech_id"],
                   "plate"=>$init_data["plate"],
                   "date"=>$init_data["date"],
                   "in_plate"=>true
               ));

                if(!plate_has_wells($plate_id,$db)){
                    //gen plate wells
                    gen_plate($init_data['date'],$init_data["tech"],$plate_id,$db);
                }

                $data = array(
                   "wells"=>fetch_plate_wells($plate_id,$init_data["plate"],$db),
                   "well_view"=>fetch_resource(_VIEWDIR."well.view"),
                   "plate"=>$init_data["plate"],
                   "plateID"=>$plate_id,
                   "techID"=>$init_data["tech_id"],
                   "init_view"=>$init_view,
                   "validated"=>plate_validated($plate_id,$db),
                   "highlight_css"=>fetch_plate_highlights($plate_id,$db),
                   "primer_list"=>$primer_list
                );

                echo load_view($view,$data);
            }else{
                echo $db["db"]->connect_error;
            }
        break;
        case "validate":
            $plate_id = sanitize_str($_GET["plateid"],$db);

            $orders = plate_validation($plate_id,$db);
            $data = array(
                "orders"=>$orders,
                "has_orders"=>(count($orders) > 0)?true:false,
                "plateID"=>$plate_id,
                "techID"=>(int)$_GET["techid"],
                "validated"=>plate_validated($plate_id,$db)
            );

            echo load_view($view,$data);
        break;
        case "init":
            if(gettype($db) != "array"){
                $data = array(
                   "employees"=>fetch_user_list($admin_branch,$db),
                   "date"=>date("Y-m-d")
                );

                echo load_view($view,$data);
            }else{
                echo $db["db"]->connect_error;
            }

        break;
        default:

        break;
    }
