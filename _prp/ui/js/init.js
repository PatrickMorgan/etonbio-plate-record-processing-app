_PRP.Init = {
    init:function(){
        this.bind();
    },
    bind:function(){
        //element event bindings
        var prpInitSubmitBtn = document.getElementById('prp-init-submit'),
            prpInitForm = document.getElementById('plate_init'),
            datePick = document.getElementById('datepick'),
            dateView = document.getElementById('dateview');

            prpInitForm.addEventListener("onsubmit",function(ev){
                ev.preventDefault();
            });
            prpInitSubmitBtn.addEventListener("click",_PRP.Init.submit);

    },
    prevent:function(ev){
        ev.preventDefault();
        return false;
    },
    submit:function(ev){
        ev.preventDefault();
        var prpInitForm = document.getElementById('plate_init');
        var loadUrl = "./?prp_view=plate&plate="+prpInitForm.plate.value+"&date="+prpInitForm.date.value+"&tech="+prpInitForm.tech.value;
            console.log([prpInitForm.plate.value,prpInitForm.date.value]);

        if(prpInitForm.plate.value.length > 0 && prpInitForm.tech.value > 0){
            window.location = loadUrl;
        }else{
            swal({
                title:"Plate Initialization Error",
                text:"Please verify "+((prpInitForm.plate.value.length < 1)?"plate":"technician")+" and try again."
            })
        }
    }
}

//fire init
_PRP.Init.init();

//on jQuery Ready
$jQ(document).ready(function(){
    //set date picker
    $jQ( "#dateview" ).datepicker({ dateFormat: "yy-mm-dd" });
});
